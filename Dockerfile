FROM golang:alpine AS build

WORKDIR /go/app

COPY app/ .

RUN go build main.go
# RUN CGO_ENABLED=0 GOOS=linux  go build main.go

FROM alpine

WORKDIR /root/

COPY --from=build /go/app/main .

EXPOSE 8080

ENTRYPOINT ./main